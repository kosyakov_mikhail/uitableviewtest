//
//  ViewController.swift
//  UITableViewTest
//
//  Created by Macos on 19.07.2022.
//

import UIKit
import SnapKit


class PersonListVC: UIViewController {
    
    
    var persons = Person.createPersons()
     
    private var tableView = UITableView()

    
// MARK: - viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if #available(iOS 13.0, *) {
//            overrideUserInterfaceStyle = .light
//            }
        tableView.register(UITableViewCell.self,
                           forCellReuseIdentifier: "cell")
        
        navigationController?.topViewController?.title = "Person List"
        navigationController?.navigationBar.titleTextAttributes =
        [.foregroundColor: UIColor.black,
         .font: UIFont(name: "Arial-BoldMT", size: 20) ?? "nil"]
        
        setupLayout()
    }

    
// MARK: - SetupLayout
    
    private func setupLayout() {
        view.addSubview(tableView)
        setTableViewDelegate()
        tableView.rowHeight = 60
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.snp.makeConstraints { make in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
            make.top.equalToSuperview()
        }
    }
    
    private func setTableViewDelegate() {
        tableView.delegate = self
        tableView.dataSource = self
    }

}

extension PersonListVC: UITableViewDelegate, UITableViewDataSource {
   
// MARK: - Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        let person = persons[indexPath.row]
        cell.textLabel?.text = person.name + " " + person.surName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = DetailInfoVC()
        detailVC.person = persons[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: false)
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
}
