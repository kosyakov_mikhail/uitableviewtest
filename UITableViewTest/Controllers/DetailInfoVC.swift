//
//  DetailInfoController.swift
//  UITableViewTest
//
//  Created by Macos on 19.07.2022.
//

import UIKit

class DetailInfoVC: UIViewController {

    
    
    var person: Person!
    
    
    var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = UIFont(name: "Arial-BoldMT", size: 25)
        return label
    }()
    
    private var emailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private var phoneLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = person.name + " " + person.surName
        emailLabel.text = "Email:  \(person.email)"
        phoneLabel.text = "Phone:  \(person.phoneNumber)"

        setupLayout()
        
//        if #available(iOS 13.0, *) {
//            overrideUserInterfaceStyle = .light
//            }
    }
    

    private func setupLayout() {
        view.translatesAutoresizingMaskIntoConstraints = true
        view.backgroundColor = .white
        
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(50)
            make.width.equalTo(300)
            make.height.equalTo(100)
        }
        
        view.addSubview(emailLabel)
        emailLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(75)
            make.left.equalToSuperview().inset(30)
            make.width.equalTo(300)
            make.height.equalTo(50)
        }
        
        view.addSubview(phoneLabel)
        phoneLabel.snp.makeConstraints { make in
            make.top.equalTo(emailLabel.snp.bottom)
            make.left.equalToSuperview().inset(30)
            make.width.equalTo(300)
            make.height.equalTo(50)
        }
    }
    
}
