//
//  PersonModel.swift
//  UITableViewTest
//
//  Created by Macos on 19.07.2022.
//

import Foundation


struct Person {
    
    var name: String
    var surName: String
    var email: String
    var phoneNumber: String
    
    static func createPersons() -> [Person] {
        
        var persons: [Person] = []
        
        let names = Data.shared.names.shuffled()
        let surnames = Data.shared.surnames.shuffled()
        let emails = Data.shared.emails.shuffled()
        let phoneNumbers = Data.shared.phoneNumbers.shuffled()
        
        for index in 0..<names.count {
            let person = Person(name: names[index],
                                surName: surnames[index],
                                email: emails[index],
                                phoneNumber: phoneNumbers[index])
            
            persons.append(person)
        }
        
        return persons
    }
}
