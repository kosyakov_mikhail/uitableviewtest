//
//  PersonData.swift
//  UITableViewTest
//
//  Created by Macos on 19.07.2022.
//

import Foundation

class Data {
    
    static let shared = Data()
    
    let names = ["John", "Peter", "Jack", "Misha", "Grigoriy", "Alexandr",
                 "Alisa", "Viktoria", "Paul", "Rodger"]
    
    let surnames = ["Smith", "Grifin", "Lord", "Apolon", "Kosyakov", "Senkin",
                    "Vigos", "Berboun", "Rassel", "Meero"]
    
    let emails = ["o@outlook.com",
                  "hr6zdl@yandex.ru",
                  "kaft93x@outlook.com",
                  "dcu@yandex.ru",
                  "19dn@outlook.com",
                  "pa5h@mail.ru",
                  "281av0@gmail.com",
                  "8edmfh@outlook.com",
                  "sfn13i@mail.ru",
                  "g0orc3x1@outlook.com"]
    
    let phoneNumbers = ["+79284515236", "+79275421478", "+79052145893",
                        "+78462051453", "+75469853214", "+79995421478",
                        "+78451425363", "+74156325896", "+74128596358",
                        "+74125869589"]
    
}
